﻿using ConsoleAppUI.Api.Authentication;
using ConsoleAppUI.Api.UserEndPoint;
using ConsoleAppUI.Models.LoggedInUser;
using DesktopUI.Library.Authentication;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace LoginTest
{
    // Kind of difficult at first because we need the API to be launched
    [TestClass]
    public class LoginTest
    {
        [TestMethod]
        [ExpectedException(typeof(Exception), "(DropboxAuthent) Déjà connecté.")]
        public async Task LoginIfAlreadyLoggedInShouldThrowException()
        {
            var loggedInUser = new LoggedInUser()
            {
                Token = "XXX"
            };

            var dropboxAuthent = new DropboxAuthent(loggedInUser);
            
            await dropboxAuthent.DoLogin("test", "test");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "(DropboxAuthent) No authent strategy.")]
        public async Task LoginIfNoStrategyForLoginThrowException()
        {
            var dropboxAuthent = new DropboxAuthent(new LoggedInUser());
            
            await dropboxAuthent.DoLogin("test", "test");
        }

        [TestMethod]
        public async Task LoginWithGoodCredentialsShouldGiveUserInformation()
        {
            var loggedInUser = new LoggedInUser();
            var dropboxAuthent = new DropboxAuthent(loggedInUser);
            var api = new ApiHelper();
            var userEndPoint = new UserEndPoint(api);
            dropboxAuthent.Strategy = new ApiAuthentication(api, userEndPoint);

            await dropboxAuthent.DoLogin("bratistouta@gmail.com", "aqwzsxAQWZSX22.");

            Assert.AreEqual("bratistouta@gmail.com", loggedInUser.Email);
        }
    }
}

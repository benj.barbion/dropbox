﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class DownloadFileModel
    {
        public FileStream Stream { get; set; }
        public string MimeType { get; set; }
        public string FileName { get; set; }
    }
}

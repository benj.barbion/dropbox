﻿using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class UserBindingModels
    {
        public struct RegisterBindingModel
        {
            [Required(AllowEmptyStrings = false)]
            [EmailAddress(ErrorMessage = "Adresse email invalide.")]
            public string Email { get; set; }

            [Required(AllowEmptyStrings = false)]
            public string Password { get; set; }

            [Required(AllowEmptyStrings = false)]
            public string ConfirmPassword { get; set; }
        }

        public struct ChangePasswordBindingModel
        {
            [Required(AllowEmptyStrings = false)]
            public string OldPassword { get; set; }

            [Required(AllowEmptyStrings = false)]
            public string NewPassword { get; set; }

            [Required(AllowEmptyStrings = false)]
            public string ConfirmPassword { get; set; }
        }
    }
}

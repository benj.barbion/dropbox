﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public struct ResponseMessageModel
    {
        public bool Success { get; }
        public string Message { get; }

        public ResponseMessageModel(bool success, string message)
        {
            this.Success = success;
            this.Message = message;
        }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace API.Models
{
    public class UploadFileModel
    {
        [FromForm(Name = "MimeType")]
        public string MimeType { get; set; }

        [FromForm(Name = "File")]
        public IFormFile File { get; set; }

        [FromForm(Name = "FileName")]
        public string FileName { get; set; }

        [FromForm(Name = "FromFileId")]
        public Guid? FromFileId { get; set; }

        [FromForm(Name = "FromDirectoryId")]
        public Guid? FromDirectoryId { get; set; }

        internal string Owner { get; set; }
    }
}

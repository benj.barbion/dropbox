﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class GroupBindingModel
    {
        public class AddGroup
        {
            [Required(AllowEmptyStrings = false)]
            [StringLength(30, MinimumLength = 6)]
            [DataType(DataType.Text)]
            [Display(Name = "Nom du groupe")]
            public string GroupName { get; set; }
        }

        public class UpdateUser
        {
            [Required(AllowEmptyStrings = false)]
            [DataType(DataType.Text)]
            [Display(Name = "Id de l'utilisateur")]
            public string UserId { get; set; }

            [Required(AllowEmptyStrings = false)]
            [DataType(DataType.Text)]
            [Display(Name = "Nouveau rôle de l'utilisateur")]
            public string Role { get; set; }
        }
    }
}

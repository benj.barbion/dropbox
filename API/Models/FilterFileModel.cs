﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class FilterFileModel
    {
        public Guid? FileId { get; set; }
        public string FileName { get; set; }
        public string Parent { get; set; }
        public string Owner { get; set; }
        public string MimeType { get; set; }
        public bool? SharedWithMe { get; set; }
        internal string From { get; set; }

        public FilterFileModel()
        {
        }

        private FilterFileModel(Builder builder)
        {
            this.FileId = builder.FileId;
            this.FileName = builder.FileName;
            this.Parent = builder.Parent;
            this.MimeType = builder.MimeType;
            this.SharedWithMe = builder.SharedWithMe;
            this.From = builder.From;
            this.Owner = builder.Owner;
        }

        public class Builder
        {
            internal Guid? FileId { get; set; }
            internal string FileName { get; set; }
            internal string Parent { get; set; }
            internal string Owner { get; set; }
            internal string MimeType { get; set; }
            internal bool? SharedWithMe { get; set; }
            internal string From { get; set; }

            public Builder SetFileId(Guid? fileId)
            {
                this.FileId = fileId;
                return this;
            }

            public Builder SetFileName(string fileName)
            {
                this.FileName = fileName;
                return this;
            }

            public Builder SetParent(string parent)
            {
                this.Parent = parent;
                return this;
            }

            public Builder SetMimeType(string mimeType)
            {
                this.MimeType = mimeType;
                return this;
            }

            public Builder SetSharedWithMe(bool? sharedWithMe)
            {
                this.SharedWithMe = sharedWithMe;
                return this;
            }

            public Builder SetFrom(string from)
            {
                this.From = from;
                return this;
            }

            public Builder SetOwner(string owner)
            {
                this.Owner = owner;
                return this;
            }

            public FilterFileModel Build() => new(this);
        }
    }
}

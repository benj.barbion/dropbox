﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class PermissionBindingModel
    {
        public class CreatePermission
        {
            [Required(AllowEmptyStrings = false)]
            [DataType(DataType.Text)]
            public string ShareWithId { get; set; }

            [Required(AllowEmptyStrings = false)]
            [DataType(DataType.Text)]
            public string RoleToAssign { get; set; }

            [Required(AllowEmptyStrings = false)]
            [DataType(DataType.Text)]
            [DefaultValue("user")]
            public string TypeOf { get; set; }
        }
    }
}
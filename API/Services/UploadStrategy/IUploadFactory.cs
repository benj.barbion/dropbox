﻿using API.Services;
using APIDataAccess.DataAccess;

namespace API.Models.UploadStrategy
{
    public interface IUploadFactory
    {
        IUploadStrategy CreateUploadStrategy(string mimeType);
    }
}
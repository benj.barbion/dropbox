﻿using API.Models;
using API.Models.UploadStrategy;
using System;
using System.Threading.Tasks;

namespace API.Services.UploadStrategy
{
    public class NullStrategy : IUploadStrategy
    {
        public Task<Guid?> Upload(UploadFileModel upload) => Task.FromResult((Guid?) null);
    }
}

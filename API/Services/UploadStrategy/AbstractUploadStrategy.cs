﻿using API.Models;
using API.Models.UploadStrategy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Services.UploadStrategy
{
    public abstract class AbstractUploadStrategy : IUploadStrategy
    {
        //Global verification of Strategy
        public void ValidateData(UploadFileModel upload)
        {
            if (upload is null)
                throw new ArgumentNullException("(AbstractUploadStrategy) Upload cannot be null !");

            if (upload.Owner is null)
                throw new Exception("(AbstractUploadStrategy) CreatorId cannot be null !");
        }
        
        public abstract Task<Guid?> Upload(UploadFileModel upload);
    }
}

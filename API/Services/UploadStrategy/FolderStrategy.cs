﻿using API.Services.FileSystem;
using API.Services.UploadStrategy;
using APIDataAccess.DataAccess;
using APIDataAccess.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models.UploadStrategy
{
    //User try to send new folder (application/folder mime type)
    public class FolderStrategy : AbstractUploadStrategy, IUploadStrategy
    {
        private readonly IFileRepository fileRepository;
        private readonly IFilePermissionService filePermissionService;
      
        public FolderStrategy(IFileRepository fileRepository, IFilePermissionService filePermissionService) 
        {
            this.fileRepository = fileRepository;
            this.filePermissionService = filePermissionService;
        }

        public override Task<Guid?> Upload(UploadFileModel upload)
        {
            base.ValidateData(upload);

            if (upload.FileName is null)
                throw new Exception("(FolderStrategyError) FileName cannot be null !");

            FileModel alreadyExist;

            //Try to send a folder inside a folder ?
            if (upload.FromDirectoryId is not null)
            {
                var file = this.fileRepository.GetById((Guid)upload.FromDirectoryId);

                if (file is null || !file.IsFolder || !this.filePermissionService.HasPerm(file.FileId, "canAddToFolder", upload.Owner))
                    throw new Exception("(FolderStrategyError) Unauthorized !");

                alreadyExist = this.fileRepository.GetByName(upload.FileName, upload.FromDirectoryId).FirstOrDefault(x => x.IsFolder);
            }
            else
                alreadyExist = this.fileRepository.GetByNameRoot(upload.FileName, upload.Owner).FirstOrDefault(x => x.IsFolder);
            
            //FolderName already exist ?
            if (alreadyExist is not null)
                throw new Exception("(FolderStrategyError) Folder already exist !");

            //We can add the folder
            var fileModel = new FileModel.Builder()
                                .SetFileName(upload.FileName)
                                .SetMimeType("application/folder")
                                .SetParentDirectory(upload.FromDirectoryId)
                                .SetFileId(Guid.NewGuid())
                                .SetOwner(upload.Owner)
                                .Build();

            return Task.FromResult(this.fileRepository.SaveFolder(fileModel) > 0 ? (Guid?)fileModel.FileId : null);
        }
    }
}

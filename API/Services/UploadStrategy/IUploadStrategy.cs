﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models.UploadStrategy
{
    public interface IUploadStrategy
    {
        Task<Guid?> Upload(UploadFileModel upload);
    }
}

﻿using API.Services.FileSystem;
using API.Services.UploadChain;
using API.Services.UploadStrategy;
using APIDataAccess.DataAccess;
using System;

namespace API.Models.UploadStrategy
{
    public class UploadFactory : IUploadFactory
    {
        private readonly IUploadChainFactory uploadChainFactory;
        private readonly IFileRepository fileRepository;
        private readonly IFilePermissionService filePermissionService;
        
        public UploadFactory(IFileRepository fr, IFilePermissionService fps, IUploadChainFactory ucf)
        {
            this.fileRepository = fr;
            this.filePermissionService = fps;
            this.uploadChainFactory = ucf;
        }

        public IUploadStrategy CreateUploadStrategy(string mimeType)
        {
            if (mimeType is null)
                return new NullStrategy();

            if (mimeType.ToLower().Equals("application/folder"))
                return new FolderStrategy(this.fileRepository, this.filePermissionService);

            return new MediaStrategy(this.uploadChainFactory);
        }
    }
}
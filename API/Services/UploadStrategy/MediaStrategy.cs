﻿using API.Services.UploadChain;
using API.Services.UploadStrategy;
using System;
using System.Threading.Tasks;

namespace API.Models.UploadStrategy
{
    //User try to send new media (application/folder mime type)
    public class MediaStrategy : AbstractUploadStrategy, IUploadStrategy
    {
        private readonly IUploadChainFactory uploadChainFactory;

        public MediaStrategy(IUploadChainFactory uploadChainFactory) 
        {
            this.uploadChainFactory = uploadChainFactory;
        }
        
        public override async Task<Guid?> Upload(UploadFileModel upload)
        {
            base.ValidateData(upload);

            //User has to upload file inside this strategy
            if (upload.File is null)
                throw new Exception("(MediaStrategyError) No file upload !");

            //Modify fileName and mimeType according to the file
            upload.FileName = upload.File.FileName;
            upload.MimeType = upload.File.ContentType;

            //Call factory to create the COR
            return await uploadChainFactory.CreateChain().Handle(upload);
        }
    }
}
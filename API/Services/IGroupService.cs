﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Services
{
    public interface IGroupService
    {
        int SaveGroup(Guid groupId, string groupName, string creatorId);
        bool HasPerm(string perm, Guid groupdId, string userId);
        int DeleteGroup(Guid groupId, string userId);
        List<string> GetPerms(Guid groupdId, string userId);
        int AddUserToGroup(Guid groupId, string userIdFrom, string userIdTo);
        int RemoveUserFromGroup(Guid groupId, string userIdFrom, string userIdTo);
    }
}

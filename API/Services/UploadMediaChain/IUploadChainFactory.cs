﻿using API.Models.UploadChain;

namespace API.Services.UploadChain
{
    public interface IUploadChainFactory
    {
        IHandler CreateChain();
    }
}
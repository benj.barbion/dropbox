﻿using API.Models.UploadChain;
using API.Services.FileSystem;
using APIDataAccess.DataAccess;

namespace API.Services.UploadChain
{
    public class UploadChainFactory : IUploadChainFactory
    {
        private readonly IFileRepository fileRepository;
        private readonly IFilePermissionService filePermissionService;
        private readonly IFileRevisionService fileRevisionService;
        private readonly IOnStorageService onStorageService;
       
        public UploadChainFactory(IFileRepository fr, IFilePermissionService fps, IFileRevisionService frs, IOnStorageService oss)
        {
            this.fileRepository = fr;
            this.filePermissionService = fps;
            this.fileRevisionService = frs;
            this.onStorageService = oss;
        }

        public IHandler CreateChain()
        {
            IHandler handler = new FileIdHandler(this.fileRevisionService);

            handler
                .SetNext(new ParentDirectoryHandler(filePermissionService, fileRepository, fileRevisionService, onStorageService))
                .SetNext(new DefaultHandler(fileRepository, fileRevisionService, onStorageService));

            return handler;
        }
    }
}

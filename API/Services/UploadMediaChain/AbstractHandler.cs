﻿using APIDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models.UploadChain
{
    public abstract class AbstractHandler : IHandler
    {
        private IHandler nextHandler;

        public IHandler SetNext(IHandler handler)
        {
            this.nextHandler = handler;
            return handler;
        }

        public FileModel.Builder PreBuildFile(UploadFileModel upload)
        {
            return new FileModel.Builder()
                        .SetFileId(Guid.NewGuid())
                        .SetFileName(upload.FileName)
                        .SetMimeType(upload.MimeType)
                        .SetOwner(upload.Owner)
                        .SetSize(upload.File.Length);
        }

        public virtual bool CanHandle(UploadFileModel upload) => upload?.Owner is not null && upload.File is not null;

        public virtual Task<Guid?> Handle(UploadFileModel upload) => this.nextHandler?.Handle(upload);
    }
}

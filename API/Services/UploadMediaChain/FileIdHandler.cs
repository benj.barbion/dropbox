﻿using API.Services.FileSystem;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models.UploadChain
{
    //User ask a file revision (FromFileId not empty)
    public class FileIdHandler : AbstractHandler, IHandler
    {
        private readonly IFileRevisionService fileRevisionService;
       
        public FileIdHandler(IFileRevisionService fileRevisionService) => this.fileRevisionService = fileRevisionService;

        public override bool CanHandle(UploadFileModel upload) => base.CanHandle(upload) && upload.FromFileId is not null;
             
        public async override Task<Guid?> Handle(UploadFileModel upload)
        {
            //Check if can handle
            if (!this.CanHandle(upload))
                return await base.Handle(upload);

            //Get PreBuild method from AbstractHandler (not inside IHandler)
            var file = this.PreBuildFile(upload).Build();

            //We juste have to call the service
            return await this.fileRevisionService.CreateRevision((Guid)upload.FromFileId, file.Owner, upload.File);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models.UploadChain
{
    public interface IHandler
    {
        IHandler SetNext(IHandler handler);
        Task<Guid?> Handle(UploadFileModel upload);
    }
}

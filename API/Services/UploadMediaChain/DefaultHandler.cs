﻿using API.Services;
using API.Services.FileSystem;
using APIDataAccess.DataAccess;
using APIDataAccess.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models.UploadChain
{
    //Default behavior (upload media in root folder)
    public class DefaultHandler : AbstractHandler, IHandler
    {
        private readonly IFileRepository fileRepository;
        private readonly IFileRevisionService fileRevisionService;
        private readonly IOnStorageService onStorageService;
        
        public DefaultHandler(IFileRepository fr, IFileRevisionService frs, IOnStorageService osv)
        {
            this.fileRepository = fr;
            this.fileRevisionService = frs;
            this.onStorageService = osv;
        }

        public override bool CanHandle(UploadFileModel upload) => base.CanHandle(upload);

        public async override Task<Guid?> Handle(UploadFileModel upload)
        {
            //Check if can handle
            if (!this.CanHandle(upload))
                return await base.Handle(upload);

            //Media name already exist inside root folder ? If yes, call revision service
            var file = this.fileRepository.GetByNameRoot(upload.FileName, upload.Owner).FirstOrDefault(x => !x.IsFolder);
            if (file is not null)
                return await this.fileRevisionService.CreateRevision(file.FileId, upload.Owner, upload.File);

            //We can create the file inside root folder
            var fileModel = this.PreBuildFile(upload).Build();

            //Awaiting the upload on storage
            await this.onStorageService.UploadFileOnStorage(fileModel.FileId, upload.File);

            //Return Guid to user
            return this.fileRepository.SaveFile(fileModel) > 0 ? (Guid?)fileModel.FileId : null;
        }
    }
}

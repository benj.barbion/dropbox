﻿using API.Services;
using API.Services.FileSystem;
using APIDataAccess.DataAccess;
using APIDataAccess.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models.UploadChain
{
    //User want to send a file inside a directory (FromDirectoryId not empty)
    public class ParentDirectoryHandler : AbstractHandler, IHandler
    {
        private readonly IFilePermissionService filePermissionService;
        private readonly IFileRepository fileRepository;
        private readonly IFileRevisionService fileRevisionService;
        private readonly IOnStorageService onStorageService;
        
        public ParentDirectoryHandler(IFilePermissionService fps, IFileRepository fr, IFileRevisionService frs, IOnStorageService osv)
        {
            this.filePermissionService = fps;
            this.fileRepository = fr;
            this.fileRevisionService = frs;
            this.onStorageService = osv;
        }

        public override bool CanHandle(UploadFileModel upload) => base.CanHandle(upload) && upload.FromDirectoryId is not null;

        public async override Task<Guid?> Handle(UploadFileModel upload)
        {
            //Check if can handle
            if (!this.CanHandle(upload))
                return await base.Handle(upload);

            var parentDirectory = (Guid)upload.FromDirectoryId;

            //Verify the ID
            var file = this.fileRepository.GetById(parentDirectory);
            if (file is null || !file.IsFolder || !this.filePermissionService.HasPerm(parentDirectory, "canAddToFolder", upload.Owner))
                throw new Exception("(ParentDirectoryHandler) Unauthorized !");

            //Media name already exist inside this folder ? If yes, call revision service
            var alreadyExist = this.fileRepository.GetByName(upload.FileName, upload.FromDirectoryId).FirstOrDefault(x => !x.IsFolder);
            if (alreadyExist is not null)
                return await this.fileRevisionService.CreateRevision(alreadyExist.FileId, upload.Owner, upload.File);

            //Get PreBuild method from AbstractHandler (not inside IHandler) and add parent
            var fileModel = this.PreBuildFile(upload).SetParentDirectory(upload.FromDirectoryId).Build();
            
            //Awaiting the upload on storage
            await this.onStorageService.UploadFileOnStorage(fileModel.FileId, upload.File);
            
            //Return Guid to user
            return this.fileRepository.SaveFile(fileModel) > 0 ? (Guid?)fileModel.FileId : null;
        }
    }
}
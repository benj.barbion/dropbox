﻿using API.Models;
using APIDataAccess.Models;
using System.Collections.Generic;

namespace API.Services.FileSystem
{
    public interface IFileListService
    {
        List<FileModel> ListFile(FilterFileModel filterFile);
    }
}
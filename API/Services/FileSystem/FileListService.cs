﻿using API.Models;
using APIDataAccess.DataAccess;
using APIDataAccess.Models;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace API.Services.FileSystem
{
    public class FileListService : IFileListService
    {
        private readonly IFileRepository fileRepository;

        public FileListService(IFileRepository fileRepository) => this.fileRepository = fileRepository;

        private bool CheckData(FilterFileModel filterFile)
        {
            if (filterFile?.From is null)
                throw new Exception("(FileListService) Fatal error : no from user.");
            
            return true;
        }

        //Seulement utilisé pour permettre à l'utilisateur de pouvoir filter ses fichiers (besoin de refactoriser)
        public List<FileModel> ListFile(FilterFileModel filterFile)
        {
            this.CheckData(filterFile);

            //Builder de prédicats
            var predicate = PredicateBuilder.New<FileModel>().And(x => true);
           
            if (filterFile.FileId is not null)
                predicate = predicate.And(x => x.FileId == filterFile.FileId); //Just like new Func<FileModel, bool>(x => x.FileId == filterFile.FileId)

            if (!string.IsNullOrEmpty(filterFile?.FileName))
                predicate = predicate.And(x => x.FileName == filterFile.FileName);

            if (filterFile.Owner is not null)
                predicate = predicate.And(x => x.Owner == filterFile.Owner);

            if (filterFile.Parent is not null)
                this.OperateParent(filterFile, ref predicate);       
           
            if (filterFile.MimeType is not null)
                predicate = predicate.And(x => x.MimeType == filterFile.MimeType);

            var result = this.fileRepository.GetAllBy(predicate, filterFile.From);

            return filterFile.SharedWithMe is not null ? this.OperateSharedWithMe(filterFile, result) : result;
        }

        private void OperateParent(FilterFileModel filterFile, ref Expression<Func<FileModel,bool>> predicate)
        {
            if (filterFile.Parent == "root")
                predicate = predicate.And(x => x.ParentDirectory == null && x.Owner == filterFile.From);
            else if (Guid.TryParse(filterFile.Parent, out Guid guid))
                predicate = predicate.And(x => x.ParentDirectory == (Guid?)guid);          
        }

        private List<FileModel> OperateSharedWithMe(FilterFileModel filterFile, List<FileModel> result)
        {
            var sharedFiles = this.fileRepository.GetSharedFiles(filterFile.From);
            
            return filterFile.SharedWithMe is true ?
                result.FindAll(x => sharedFiles.Contains(x.FileId)) :
                    result.FindAll(x => !sharedFiles.Contains(x.FileId));
        }
    }
}

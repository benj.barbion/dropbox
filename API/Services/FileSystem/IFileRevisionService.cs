﻿using APIDataAccess.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Services.FileSystem
{
    public interface IFileRevisionService
    {
        Task<Guid?> CreateRevision(Guid fileId, string creatorId, IFormFile upload);
        bool Delete(Guid fileId, Guid revisionId, string user);
        List<FileRevision> List(Guid fileId, string user);
    }
}
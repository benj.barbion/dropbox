﻿using API.Models;
using API.Models.UploadStrategy;
using APIDataAccess.DataAccess;
using System;
using System.Threading.Tasks;

namespace API.Services.FileSystem
{
    public class FileCreatorService : IFileCreatorService
    {
        private readonly IUploadFactory uploadFactory;
        private readonly IFilePermissionService filePermissionService;
        private readonly IOnStorageService onStorageService;
        private readonly IFileRepository fileRepository;

        public IUploadStrategy UploadStrategy { get; set; }

        public FileCreatorService(IUploadFactory uploadFactory, IFilePermissionService filePermissionService, IOnStorageService onStorageService, IFileRepository fileRepository)
        {
            this.uploadFactory = uploadFactory;
            this.filePermissionService = filePermissionService;
            this.onStorageService = onStorageService;
            this.fileRepository = fileRepository;
        }
                   
        public async Task<Guid?> Create(UploadFileModel uploadFile)
        {
            if (uploadFile is null)
                throw new ArgumentNullException("Upload file cannot be null.");

            if (uploadFile.MimeType is null)
                uploadFile.MimeType = uploadFile.File?.ContentType;

            this.UploadStrategy = this.uploadFactory.CreateUploadStrategy(uploadFile.MimeType);

            return await this.UploadStrategy?.Upload(uploadFile);
        }

        public bool Delete(Guid fileId, string user)
        {
            if (!this.filePermissionService.HasPerm(fileId, "canDelete", user))
                throw new Exception("Unauthorized.");

            try
            {
                return this.fileRepository.DeleteFile(fileId) > 0 && this.onStorageService.DeleteFileFromStorage(fileId);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

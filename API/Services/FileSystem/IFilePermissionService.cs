﻿using System;
using System.Collections.Generic;

namespace API.Services.FileSystem
{
    public interface IFilePermissionService
    {
        bool CreatePermission(string userIdFrom, string userIdTo, string role, Guid fileId, bool shareWithGroup = false);
        List<string> GetPerms(Guid fileId, string userId);
        bool HasPerm(Guid fileId, string perm, string userId);
    }
}
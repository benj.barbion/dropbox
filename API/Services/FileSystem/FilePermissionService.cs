﻿using APIDataAccess.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Services.FileSystem
{
    public class FilePermissionService : IFilePermissionService
    {
        private readonly IFileRepository fileRepository;

        public FilePermissionService(IFileRepository fileRepository) => this.fileRepository = fileRepository;

        public bool HasPerm(Guid fileId, string perm, string userId) => this.GetPerms(fileId, userId).Contains(perm);

        public List<string> GetPerms(Guid fileId, string userId)
        {
            var perms = new List<string>();
            var currentId = fileId;
            
            do
            {
                perms.AddRange(this.fileRepository.GetPerms(currentId, userId));

                var guid = this.fileRepository.GetById(currentId)?.ParentDirectory;
                if (guid is null)
                    break;

                currentId = (Guid)guid;
            }
            while (perms.Count == 0);

            return perms;
        }

        public bool CreatePermission(string userIdFrom, string userIdTo, string role, Guid fileId, bool shareWithGroup = false)
        {
            var file = this.fileRepository.GetById(fileId);

            if (this.fileRepository.GetRoles().FirstOrDefault(x => x.Name == role) is null)
                throw new Exception("(FilePermissionService) Unauthorized. Are you sure about the role name ?");
            
            if (file is null)
                throw new Exception("(FilePermissionService) File not found.");

            if (!this.HasPerm(fileId, "canShare", userIdFrom))
                throw new Exception("(FilePermissionService) Unauthorized.");

            if (this.fileRepository.SavePermission(userIdFrom, userIdTo, role, fileId, shareWithGroup) == 0)
                throw new Exception("(FilePermissionService) Problem during insertion. Are you sure about the user ID ?");

            return true;
        }
    }
}

﻿using API.Models;
using System;
using System.Threading.Tasks;

namespace API.Services.FileSystem
{
    public interface IFileCreatorService
    {
        Task<Guid?> Create(UploadFileModel uploadFile);
        bool Delete(Guid fileId, string user);
    }
}
﻿using APIDataAccess.DataAccess;
using APIDataAccess.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Services.FileSystem
{
    public class FileRevisionService : IFileRevisionService
    {
        private readonly IFileRepository fileRepository;
        private readonly IFilePermissionService filePermissionService;
        private readonly IOnStorageService onStorageService;
        
        public FileRevisionService(IFileRepository fileRepository, IFilePermissionService filePermissionService, IOnStorageService onStorageService)
        {
            this.fileRepository = fileRepository;
            this.filePermissionService = filePermissionService;
            this.onStorageService = onStorageService;
        }

        public async Task<Guid?> CreateRevision(Guid fileId, string creatorId, IFormFile upload)
        {
            var file = this.fileRepository.GetById(fileId);
            if (file is null)
                throw new Exception("(FileRevisionService) File not found.");

            if (file.IsFolder is true)
                throw new Exception("(FileRevisionService) You cannot create revision of directory.");

            if (!this.filePermissionService.HasPerm(file.FileId, "canModify", creatorId))
                throw new Exception("(FileRevisionService) Unauthorized.");

            var guid = Guid.NewGuid();

            if (!await this.onStorageService.UploadFileOnStorage(guid, upload))
                throw new Exception("(FileRevisionService) Error during upload, try again later.");

            var fileBuilder = new FileModel.Builder()
                                 .SetFileId(guid)
                                 .SetOwner(creatorId)
                                 .SetSize(upload.Length)
                                 .SetRevisionOf(file.FileId)
                                 .Build();

            int insertion = this.fileRepository.SaveRevision(fileBuilder);

            return insertion > 0 ? (Guid?)guid : null;
        }

        public bool Delete(Guid fileId, Guid revisionId, string user)
        {
            if (!this.filePermissionService.HasPerm(fileId, "canModify", user))
                throw new Exception("Unauthorized.");

            if (this.fileRepository.GetAllRevisionFor(fileId).Count == 1)
                throw new Exception("This is the only revision, you cannot delete it.");

            return this.fileRepository.DeleteRevision(revisionId) > 0 && this.onStorageService.DeleteFileFromStorage(revisionId);
        }

        public List<FileRevision> List(Guid fileId, string user)
        {
            if (!this.filePermissionService.HasPerm(fileId, "canModify", user))
                throw new Exception("What are you trying to do ?");

            return this.fileRepository.GetAllRevisionFor(fileId);
        }
    }
}

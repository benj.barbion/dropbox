﻿using APIDataAccess.Repositories;
using System;
using System.Collections.Generic;

namespace API.Services
{
    public class GroupService : IGroupService
    {
        private readonly IGroupRepository groupRepository;

        public GroupService(IGroupRepository groupRepository) => this.groupRepository = groupRepository;

        public int AddUserToGroup(Guid groupId, string userIdFrom, string userIdTo)
        {
            return this.HasPerm("canAddUser", groupId, userIdFrom) ? this.groupRepository.AddUserToGroup(groupId, userIdTo) : 0;
        }

        public int DeleteGroup(Guid groupId, string userId)
        {
            return this.HasPerm("canDeleteGroup", groupId, userId) ? this.groupRepository.DeleteGroup(groupId) : 0;
        }

        public List<string> GetPerms(Guid groupdId, string userId)
        {
            return this.groupRepository.GetPerms(groupdId, userId);
        }

        public bool HasPerm(string perm, Guid groupId, string userId)
        {
            var perms = this.GetPerms(groupId, userId);

            return perms.Contains(perm);
        }

        public int RemoveUserFromGroup(Guid groupId, string userIdFrom, string userIdTo)
        {
            return this.HasPerm("canRemoveUser", groupId, userIdFrom) ? this.groupRepository.RemoveUserFromGroup(groupId, userIdTo) : 0;
        }

        public int SaveGroup(Guid groupId, string groupName, string creatorId)
        {
            return this.groupRepository.SaveGroup(groupId, groupName, creatorId);
        }
    }
}

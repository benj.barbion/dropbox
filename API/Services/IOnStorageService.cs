﻿using API.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace API.Services
{
    public interface IOnStorageService
    {
        Task<bool> UploadFileOnStorage(Guid id, IFormFile file);
        DownloadFileModel DownloadFileFromStorage(Guid fileId, string userId);
        bool DeleteFileFromStorage(Guid fileId);
    }
}
﻿using API.Models;
using API.Services.FileSystem;
using APIDataAccess.DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace API.Services
{
    public class OnStorageService : IOnStorageService
    {
        private readonly IConfiguration config;
        private readonly IFilePermissionService filePermissionService;
        private readonly IFileRepository fileRepository;
        
        public OnStorageService(IConfiguration config, IFilePermissionService filePermissionService, IFileRepository fileRepository)
        {
            this.config = config;
            this.filePermissionService = filePermissionService;
            this.fileRepository = fileRepository;
        }

        private string GetPath()
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), this.config.GetValue<string>("Secrets:Storage"));
            
            if (!Directory.Exists(path))
                throw new Exception("Fatal error exception !");

            return path;
        }

        public async Task<bool> UploadFileOnStorage(Guid id, IFormFile file)
        {
            using var fileStream = new FileStream(Path.Combine(this.GetPath(), $"{id}"), FileMode.Create);
            fileStream.Position = 0;
            
            await file.CopyToAsync(fileStream);
            
            return true;
        }

        public DownloadFileModel DownloadFileFromStorage(Guid fileId, string userId)
        {
            if (!this.filePermissionService.HasPerm(fileId, "canDownload", userId))
                throw new Exception("Unauthorized.");

            var path = Path.Combine(Directory.GetCurrentDirectory(), config.GetValue<string>("Secrets:Storage"), $"{fileId}");

            var file = this.fileRepository.GetById(fileId);

            return new DownloadFileModel()
            {
                Stream = File.OpenRead(path),
                FileName = file.FileName,
                MimeType = file.MimeType
            };
        }

        public bool DeleteFileFromStorage(Guid fileId)
        {
            File.Delete(Path.Combine(Directory.GetCurrentDirectory(), config.GetValue<string>("Secrets:Storage"), $"{fileId}"));

            return true;
        }
    }
}

﻿using API.Models;
using APIDataAccess.DataAccess;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Security.Claims;
using System.Threading.Tasks;

// HTTPGET (READ) --> [FromRoute] --> api/User/x, [FromQuery] --> api/User?x=
// HTTPOST (CREATE) / HTTPPUT (UPDATE) --> [FromBody] --> application/json, [FromForm] --> application/x-www-url-formencoded
// HTTPDELETE (DELETE) --> [FromRoute]
// Optionnal : [FromServices]
// NotFound() || Ok(product) || BadRequest() || CreatedAtAction(nameof(GetById), new { id = product.Id }, product) || Unauthorized()

namespace API.Controllers
{
    [Route("api/[controller]"), ApiController, Authorize]
	public class UserController : ControllerBase
    {
		private readonly UserManager<IdentityUser> userManager;
		private readonly IUserRepository userData;

		public UserController(UserManager<IdentityUser> userManager, IUserRepository userData)
		{
			this.userData = userData;
			this.userManager = userManager;
		}

		[SwaggerOperation(Summary = "Enregistrer un compte")]
		[AllowAnonymous, HttpPost("Register")]
		public async Task<IActionResult> Register([FromBody] UserBindingModels.RegisterBindingModel model)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			IdentityUser identity = new()
			{
				UserName = model.Email,
				Email = model.Email,
			};

			var result = await userManager.CreateAsync(identity, model.Password);
			if (!result.Succeeded)
				return BadRequest(this.BasicMessage(false, "Erreur durant l'inscription."));

			return Ok(this.BasicMessage(false, "Création avec succès."));
		}

		[SwaggerOperation(Summary = "Modifier le mot de passe")]
		[HttpPut("ChangePassword")]
		public async Task<IActionResult> ChangePassword([FromBody] UserBindingModels.ChangePasswordBindingModel model)
		{
			var user = await userManager.FindByIdAsync(User.FindFirstValue(ClaimTypes.NameIdentifier));

			var result = await userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
			if (!result.Succeeded)
				return BadRequest(result);
			
			return Ok(this.BasicMessage(true, "Mot de passe modifié avec succès."));
		}

		[SwaggerOperation(Summary = "Récupérer vos informations d'utilisateur")]
		[HttpGet]
		public IActionResult GetMe() => Ok(this.userData.GetUser(User.FindFirstValue(ClaimTypes.NameIdentifier)));
	}
}

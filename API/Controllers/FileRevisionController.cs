﻿using API.Services.FileSystem;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Security.Claims;

namespace API.Controllers
{
    [Route("api/File"), ApiController, Authorize]
    public class FileRevisionController : ControllerBase
    {
        private readonly IFileRevisionService fileRevisionService;

        public FileRevisionController(IFileRevisionService fileRevisionService) => this.fileRevisionService = fileRevisionService;

        [SwaggerOperation(Summary = "Supprimer une révision")]
        [HttpDelete("{fileId}/revisions/{revisionId}")]
        public IActionResult DeleteRevision([FromRoute] Guid fileId, [FromRoute] Guid revisionId)
        {
            try
            {
                this.fileRevisionService.Delete(fileId, revisionId, User.FindFirstValue(ClaimTypes.NameIdentifier));
                return Ok(this.BasicMessage(true, "Opération effectuée avec succès."));
            }
            catch (Exception e)
            {
                return BadRequest(this.BasicMessage(false, e.Message));
            }
        }

        [SwaggerOperation(Summary = "Lister les révisions")]
        [HttpGet("{fileId}/revisions")]
        public IActionResult ListRevisions([FromRoute] Guid fileId)
        {
            try
            {
                return Ok(this.fileRevisionService.List(fileId, User.FindFirstValue(ClaimTypes.NameIdentifier)));
            }
            catch (Exception e)
            {
                return BadRequest(this.BasicMessage(false, e.Message));
            }
        }
    }
}

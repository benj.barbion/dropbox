﻿using API.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public static class ControllerExtensions
    {
        public static ResponseMessageModel BasicMessage(this ControllerBase controller, bool success, string message)
        {
            return new ResponseMessageModel(success, message);
        }
    }
}

﻿using API.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class TokenController : Controller
    {
        private readonly ApplicationDbContext context;
        private readonly UserManager<IdentityUser> userManager;
        private readonly IConfiguration config;

        public TokenController(ApplicationDbContext context, UserManager<IdentityUser> userManager, IConfiguration config)
        {
            this.context = context;
            this.userManager = userManager;
            this.config = config;
        }

        [SwaggerOperation(Summary = "Se connecter à l'API (Bearer Token)")]
        [HttpPost, Route("/JWTToken")]
        public async Task<IActionResult> Create(string username, string password)
        {
            bool valid = await this.userManager.CheckPasswordAsync(await this.userManager.FindByEmailAsync(username), password);

            if (valid)
            {
                var result = await GenerateToken(username);
                
                return Ok(result);
            }

            return BadRequest("Erreur d'identifiants.");
        }

        private async Task<string> GenerateToken(string username)
        {
            var user  = await this.userManager.FindByEmailAsync(username);
            
            var roles = from ur in this.context.UserRoles
                        join r in this.context.Roles on ur.RoleId equals r.Id
                        where ur.UserId == user.Id
                        select new { ur.UserId, ur.RoleId, r.Name };

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, username),
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(JwtRegisteredClaimNames.Nbf, new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds().ToString()),
                new Claim(JwtRegisteredClaimNames.Exp, new DateTimeOffset(DateTime.Now.AddDays(1)).ToUnixTimeSeconds().ToString())
            };

            foreach (var role in roles)
                claims.Add(new Claim(ClaimTypes.Role, role.Name));
            
            var token = new JwtSecurityToken(
                new JwtHeader(
                    new SigningCredentials(
                        new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.config.GetValue<string>("Secrets:SecurityKey"))
                    ),
                    SecurityAlgorithms.HmacSha256)
                ),
                new JwtPayload(claims)
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}

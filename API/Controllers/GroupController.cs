﻿using API.Models;
using API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Security.Claims;

namespace API.Controllers
{
    [Route("api/[controller]"), ApiController, Authorize]
    public class GroupController : ControllerBase
    {
        private readonly IGroupService groupService;

        public GroupController(IGroupService groupService) => this.groupService = groupService;

        [SwaggerOperation(Summary = "Créer un nouveau groupe")]
        [HttpPost]
        public IActionResult Create([FromBody] GroupBindingModel.AddGroup addGroup)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var groupId = Guid.NewGuid();

            try
            {
                var insertGroup = this.groupService.SaveGroup(groupId, addGroup.GroupName, userId);

                if (insertGroup > 0)
                    return Ok(new { Success = true, GroupId = groupId });
                else
                    return BadRequest(new { Title = "Ajout impossible", Detail = "Le nom de groupe existe déjà." });
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [SwaggerOperation(Summary = "Supprimer un groupe")]
        [HttpDelete("{idGroup}")]
        public IActionResult Delete([FromRoute] string idGroup)
        {
            try
            {
                var groupGuid = new Guid(idGroup);
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

                var deleteGroup = this.groupService.DeleteGroup(groupGuid, userId);

                if (deleteGroup > 0)
                    return Ok(new { Success = true, Detail = "Le groupe a été supprimé avec succès" });
                else
                    return BadRequest(new { Title = "Suppression impossible", Detail = "Le groupe ne peut pas être supprimé." });
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }      
        }

        [SwaggerOperation(Summary = "Lister les permissions de l'utilisateur au groupe")]
        [HttpGet("{idGroup}/permissions")]
        public IActionResult GetPerms([FromRoute] string idGroup)
        {
            try
            {
                return Ok(this.groupService.GetPerms(new Guid(idGroup), User.FindFirstValue(ClaimTypes.NameIdentifier)));
            }
            catch (Exception)
            {
                return BadRequest(new { Success = false, Title = "Opération impossible."});
            } 
        }

        [SwaggerOperation(Summary = "Ajouter un utilisateur au groupe")]
        [HttpPost("{idGroup}")]
        public IActionResult AddUser([FromRoute] string idGroup, [FromForm] string userId)
        {
            try
            {
                var addUserToGroup = this.groupService.AddUserToGroup(new Guid(idGroup), User.FindFirstValue(ClaimTypes.NameIdentifier), userId);
                             
                if (addUserToGroup > 0)
                    return Ok(new { Success = true, Message = "L'utilisateur a été ajouté avec succès." });
                else
                    return BadRequest(new { Message = "Ajout de l'utilisateur impossible." });
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [SwaggerOperation(Summary = "Supprimer un utilisateur au groupe")]
        [HttpDelete("{idGroup}/{userId}")]
        public IActionResult RemoveUser([FromRoute] string idGroup, [FromRoute] string userId)
        {
            try
            {
                var removeUserFromGroup = this.groupService.RemoveUserFromGroup(new Guid(idGroup), User.FindFirstValue(ClaimTypes.NameIdentifier), userId);

                if (removeUserFromGroup > 0)
                    return Ok(new { Success = true, Message = "L'utilisateur a été supprimé avec succès." });
                else
                    return BadRequest(new { Message = "Suppression de l'utilisateur impossible." });
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}

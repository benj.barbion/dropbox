﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using API.Models;
using Swashbuckle.AspNetCore.Annotations;
using System;
using API.Services.FileSystem;
using API.Services;

namespace API.Controllers
{
    [Route("api/[controller]"), ApiController, Authorize]
    public class FileController : ControllerBase
    {
        [SwaggerOperation(Summary = "Créer un nouveau fichier")]
        [DisableRequestSizeLimit, HttpPost]
        public async Task<IActionResult> Create([FromForm] UploadFileModel uploadFile, [FromServices] IFileCreatorService fileCreator)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            uploadFile.Owner = User.FindFirstValue(ClaimTypes.NameIdentifier);

            try
            {
                var guid = await fileCreator.Create(uploadFile);
                if (guid is null)
                    return BadRequest(this.BasicMessage(false, "Erreur dans les données."));
                
                return Ok(guid);
            }
            catch (Exception e)
            {
                return BadRequest(this.BasicMessage(false, e.Message));
            }
        }

        [SwaggerOperation(Summary = "Lister tous les fichiers (selon certains filtres)")]
        [HttpGet]
        public IActionResult List([FromQuery] FilterFileModel listFile, [FromServices] IFileListService fileListService)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            listFile.From = User.FindFirstValue(ClaimTypes.NameIdentifier);

            try
            {
                return Ok(fileListService.ListFile(listFile));
            }
            catch (Exception e)
            {
                return BadRequest(this.BasicMessage(false, e.Message));
            }
        }

        [SwaggerOperation(Summary = "Supprimer un fichier")]
        [HttpDelete("{fileId}")]
        public IActionResult Delete([FromRoute] Guid fileId, [FromServices] IFileCreatorService fileCreator)
        {
            try
            {
                fileCreator.Delete(fileId, User.FindFirstValue(ClaimTypes.NameIdentifier));
                
                return Ok(this.BasicMessage(false, "Suppression réalisée avec succès."));
            }
            catch (Exception e)
            {
                return Ok(this.BasicMessage(false, e.Message));
            }   
        }

        [SwaggerOperation(Summary = "Télécharger le fichier")]
        [HttpGet("{fileId}")]
        public IActionResult Get([FromRoute] Guid fileId, [FromServices] IOnStorageService onStorageService)
        {
            try
            {
                var file = onStorageService.DownloadFileFromStorage(fileId, User.FindFirstValue(ClaimTypes.NameIdentifier));

                var res = File(file.Stream, file.MimeType, file.FileName);
                res.EnableRangeProcessing = true;

                return res;
            }
            catch (Exception e)
            {
                return BadRequest(this.BasicMessage(false, e.Message));
            }
        }
    }
}

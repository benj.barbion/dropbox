﻿using API.Models;
using API.Services.FileSystem;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Security.Claims;

namespace API.Controllers
{
    [Route("api/File"), ApiController, Authorize]
    public class FilePermissionController : ControllerBase
    {
        private readonly IFilePermissionService filePermissionService;

        public FilePermissionController(IFilePermissionService filePermissionService) => this.filePermissionService = filePermissionService;

        [SwaggerOperation(Summary = "Créer une permission")]
        [HttpPost("{fileId}/permissions")]
        public IActionResult CreatePermission([FromRoute] Guid fileId, [FromBody] PermissionBindingModel.CreatePermission perm)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            try
            {
                this.filePermissionService.CreatePermission(userId, perm.ShareWithId, perm.RoleToAssign, fileId, perm.TypeOf.Equals("group"));
                
                return Ok(new { Success = true, Message = "Pemission ajoutée avec succès." });
            }
            catch (Exception e)
            {
                return BadRequest(new { Success = false, e.Message });
            }
        }

        [SwaggerOperation(Summary = "Supprimer une permission")]
        [HttpDelete("{fileId}/permissions/{permissionId}")]
        public void DeletePermission([FromRoute] string fileId, [FromRoute] string permissionId)
        {

        }

        [SwaggerOperation(Summary = "Lister les permissions")]
        [HttpGet("{fileId}/permissions")]
        public IActionResult ListPermissions([FromRoute] Guid fileId)
        {
            return Ok(this.filePermissionService.GetPerms(fileId, User.FindFirstValue(ClaimTypes.NameIdentifier)));
        }
    }
}

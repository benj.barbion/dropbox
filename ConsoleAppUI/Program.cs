﻿using ConsoleAppUI.Api.Authentication;
using ConsoleAppUI.Api.UserEndPoint;
using ConsoleAppUI.Library.Services;
using ConsoleAppUI.Models.LoggedInUser;
using ConsoleAppUI.Scenarios;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ConsoleAppUI
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<ILoggedInUser, LoggedInUser>()
                .AddSingleton<IApiHelper, ApiHelper>()
                .AddSingleton<IUploadFileService, UploadFileService>()
                .AddTransient<IFileEndPoint, FileEndPoint>()
                .AddTransient<IUserEndPoint, UserEndPoint>()
                .AddTransient<IDropboxAuthent, DropboxAuthent>()           
                .AddTransient<AuthenticationFactory>()
                .AddTransient<IScenario, LoginRegisterScenario>()
                .AddTransient<IScenario, ListFileScenario>()
                .AddTransient<IScenario, OtherScenarioAPI>()
                .BuildServiceProvider();

            //I can also do a Factory to avoid multiple IScenario in serviceProvider
            var scenarios = serviceProvider.GetServices<IScenario>();
            
            Console.WriteLine("Veuillez attendre le chargement de l'api...\n\n");

            await scenarios.First(o => o.GetType() == typeof(LoginRegisterScenario)).Start();

            await scenarios.First(o => o.GetType() == typeof(ListFileScenario)).Start();

            await scenarios.First(o => o.GetType() == typeof(OtherScenarioAPI)).Start();
        }
    }
}

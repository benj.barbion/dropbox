﻿using ConsoleAppUI.Api.Authentication;
using ConsoleAppUI.Api.UserEndPoint;
using ConsoleAppUI.Models.LoggedInUser;
using DesktopUI.Library.Authentication;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace ConsoleAppUI.Scenarios
{
    class LoginRegisterScenario : IScenario
    {
        private readonly IDropboxAuthent authentication;
        private readonly IUserEndPoint userEndPoint;
        private readonly ILoggedInUser loggedInUser;
        private readonly AuthenticationFactory authenticationFactory;

        public LoginRegisterScenario(IDropboxAuthent authentication, AuthenticationFactory authenticationFactory, IUserEndPoint userEndPoint, ILoggedInUser loggedInUser)
        {
            this.authentication = authentication;
            this.authenticationFactory = authenticationFactory;
            this.userEndPoint = userEndPoint;
            this.loggedInUser = loggedInUser;
        }

        public async Task Login(string user, string password)
        {
            try
            {
                Console.WriteLine("Connexion en cours...");
                await this.authentication.DoLogin(user, password);
                Console.WriteLine("Connecté avec succès !");
                Console.WriteLine(JsonConvert.SerializeObject(this.loggedInUser, Formatting.Indented));
                //JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void Logout()
        {
            try
            {
                Console.WriteLine("Déconnexion en cours...");
                this.authentication.DoLogout();
                Console.WriteLine("Déconnecté avec succès.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public async Task Register(string email, string password, string confirmPass)
        {
            try
            {
                Console.WriteLine("Inscription en cours...");
                await this.userEndPoint.Register(email, password, confirmPass);
                Console.WriteLine("Inscription réalisée avec succès.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public async Task Start()
        {
            Console.WriteLine("Début du scénario LoginRegister", Environment.NewLine);

            this.authentication.Strategy = this.authenticationFactory.CreateAuthentication(nameof(ApiAuthentication));

            var aleaGuid = Guid.NewGuid();

            await this.Login($"{aleaGuid}@gmail.com", "test");

            Console.WriteLine(Environment.NewLine);

            await this.Register($"{aleaGuid}@gmail.com", "aqwzsxAQWZSX22..", "aqwzsxAQWZSX22..");

            Console.WriteLine(Environment.NewLine);

            await this.Register($"{aleaGuid}@gmail.com", "aqwzsxAQWZSX22..", "aqwzsxAQWZSX22..");

            Console.WriteLine(Environment.NewLine);

            await this.Login($"{aleaGuid}@gmail.com", "aqwzsxAQWZSX22..");

            this.Logout();
        }
    }
}

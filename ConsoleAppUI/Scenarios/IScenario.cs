﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppUI.Scenarios
{
    interface IScenario
    {
        Task Start();
    }
}

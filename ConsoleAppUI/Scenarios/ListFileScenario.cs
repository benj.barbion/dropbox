﻿using ConsoleAppUI.Api.Authentication;
using ConsoleAppUI.Api.UserEndPoint;
using ConsoleAppUI.Models.LoggedInUser;
using DesktopUI.Library.Authentication;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace ConsoleAppUI.Scenarios
{
    class ListFileScenario : IScenario
    {
        private readonly IDropboxAuthent authentication;
        private readonly ILoggedInUser loggedInUser;
        private readonly IFileEndPoint fileEndPoint;
        private readonly AuthenticationFactory authenticationFactory;

        public ListFileScenario(IDropboxAuthent auth, ILoggedInUser logg, IFileEndPoint fep, AuthenticationFactory authF)
        {
            this.authentication = auth;
            this.authenticationFactory = authF;
            this.loggedInUser = logg;
            this.fileEndPoint = fep;
        }

        public async Task Start()
        {
            //Pattern not really necessary in our case
            this.authentication.Strategy = this.authenticationFactory.CreateAuthentication(nameof(ApiAuthentication));

            await this.authentication.DoLogin("test@test.com", "aqwzsxAQWZSX22.");

            var root = await this.fileEndPoint.ListFile("Parent=root");
            Console.WriteLine($"\nVous avez {root.Count} fichiers dans votre root.\n");
            Console.WriteLine(JsonConvert.SerializeObject(root, Formatting.Indented));

            var parent = await this.fileEndPoint.ListFile("Parent=cd65aab2-8e6d-491d-b112-47faa834c313");
            Console.WriteLine($"\nVous avez {parent.Count} fichiers dans le dossier cd65aab2-8e6d-491d-b112-47faa834c313.\n");
            Console.WriteLine(JsonConvert.SerializeObject(parent, Formatting.Indented));

            var shared = await this.fileEndPoint.ListFile("SharedWithMe=true");
            Console.WriteLine($"\nVous avez {shared.Count} fichiers qui ont été partagés avec vous.\n");
            Console.WriteLine(JsonConvert.SerializeObject(shared, Formatting.Indented));

            //Un peu de fun ?
            var mineFolder = await this.fileEndPoint.ListFile($"MimeType=application/folder&Owner={this.loggedInUser.Id}");
            Console.WriteLine($"\nVous avez {mineFolder.Count} dossiers créés par vous.\n");
            Console.WriteLine(JsonConvert.SerializeObject(mineFolder, Formatting.Indented));

            var revisionFor = await this.fileEndPoint.GetAllRevisionFor(new Guid("4bdaf315-b1bf-48d3-8d65-01a056aaf0bf"));
            Console.WriteLine($"\nVous avez {revisionFor.Count} révisions pour le fichier 4bdaf315-b1bf-48d3-8d65-01a056aaf0bf.\n");
            Console.WriteLine(JsonConvert.SerializeObject(revisionFor, Formatting.Indented));

            //Utilisez l'api si l'envie venait d'effectuer + de tests :)
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppUI.Scenarios
{
    class OtherScenarioAPI : IScenario
    {
        public Task Start()
        {
            Console.WriteLine("\n\nIl ne vous reste plus qu'à vous amuser avec la version graphique de l'API.");
            Console.WriteLine("\nEnvoi de fichier, téléchargement, gestion des groupes, etc. :)");
            Console.WriteLine("\n\n2 comptes existants possédants plusieurs fichiers : ");
            Console.WriteLine("\nbratistouta@gmail.com");
            Console.WriteLine("aqwzsxAQWZSX22.");
            Console.WriteLine("\n-------------------------");
            Console.WriteLine("\ntest@test.com");
            Console.WriteLine("aqwzsxAQWZSX22.");

            return Task.CompletedTask;
        }
    }
}

﻿CREATE PROCEDURE [dbo].[GetPermForFile]
	@FileId UNIQUEIDENTIFIER,
	@UserId NVARCHAR(450)
AS
BEGIN
	IF @UserId = (SELECT IdCreator FROM dbo.[File] WHERE FileId = @FileId)
		SELECT [Name] FROM dbo.FilePermission
		
	ELSE	
		DECLARE @FileIdInt INT = (SELECT Id FROM dbo.[File] WHERE FileId = @FileId)

		SELECT DISTINCT fp.Name
		FROM dbo.FileSharing fs
		INNER JOIN dbo.FileRolePermission frp ON frp.IdRole = fs.IdRole
		INNER JOIN dbo.FilePermission fp ON fp.Id = frp.IdPermission
		WHERE IdToShare = @FileIdInt AND (IdUserTo = @UserId OR IdGroupTo IN (SELECT IdGroup FROM dbo.[GroupUser] WHERE IdUser = @UserId))
END
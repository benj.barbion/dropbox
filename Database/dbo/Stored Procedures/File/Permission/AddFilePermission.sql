﻿CREATE PROCEDURE [dbo].[AddFilePermission]
	@UserIdFrom NVARCHAR(450), 	
	@UserIdTo NVARCHAR(450), 	
	@Role NVARCHAR(200), 
	@FileId UNIQUEIDENTIFIER,
	@SharedWithGroup BIT = 0
AS
BEGIN	
	SET IMPLICIT_TRANSACTIONS ON

	BEGIN TRY
		DECLARE @FileIdInt INT = (SELECT Id FROM [dbo].[File] WHERE FileId = @FileId)
		DECLARE @RoleId INT = (SELECT Id FROM [dbo].[FileRole] WHERE Name = @Role)

		if @SharedWithGroup = 1 
		BEGIN
			DECLARE @GroupID INT = (SELECT Id FROM dbo.[Group] WHERE GroupId = @UserIdTo);
			INSERT INTO [dbo].[FileSharing](IdToShare, IdUserFrom, IdGroupTo, IdRole) VALUES (@FileIdInt, @UserIdFrom, @GroupID, @RoleId)
		END	
		ELSE 
			INSERT INTO [dbo].[FileSharing](IdToShare, IdUserFrom, IdUserTo, IdRole) VALUES (@FileIdInt, @UserIdFrom, @UserIdTo, @RoleId)

		COMMIT 		
	END TRY

	BEGIN CATCH
		ROLLBACK
	END CATCH	
END
﻿CREATE PROCEDURE [dbo].[GetAllFiles]
	@UserId NVARCHAR(450)
AS
BEGIN
	with parent_cte (Id, FileId, FileName, CreateDate, Owner, Size, MimeType, Trashed, ParentDirectory, IsFolder) as (
		SELECT f.Id, f.FileId, f.Name AS FileName, f.CreateDate, f.IdCreator AS Owner, fr.Size, f.MimeType, f.IsDeleted AS Trashed, f2.FileId AS ParentDirectory, f.IsFolder
		FROM dbo.[File] f
		LEFT JOIN dbo.[File] f2 ON f2.Id = f.IdParentFolder
		LEFT JOIN dbo.[FileRevision] fr ON fr.RevisionOf = f.Id AND fr.Id = f.CurrentRevisionId
		WHERE f.IdCreator = @UserId 
			OR 
				f.FileId IN (SELECT f.FileId
							FROM dbo.FileSharing fs
							INNER JOIN dbo.[File] f ON f.Id = fs.IdToShare 
							WHERE f.IdCreator != @UserId 
								AND (fs.IdUserTo = @UserId 
										OR fs.IdGroupTo 
											IN (SELECT IdGroup FROM dbo.GroupUser WHERE IdUser = @UserId)))

		UNION ALL
  
		SELECT f.Id, f.FileId, f.Name AS FileName, f.CreateDate, f.IdCreator AS Owner, B.Size, f.MimeType, f.IsDeleted AS Trashed, A.FileId AS ParentDirectory, f.IsFolder
		FROM dbo.[File] f
		OUTER APPLY (SELECT f2.FileId AS FileId FROM dbo.[File] f2 WHERE f2.Id = f.IdParentFolder) A
		OUTER APPLY (SELECT fr.Size as Size FROM dbo.[FileRevision] fr WHERE fr.RevisionOf = f.Id AND fr.Id = f.CurrentRevisionId) B

		INNER JOIN parent_cte pc on f.IdParentFolder = pc.Id
	)

	SELECT DISTINCT * FROM parent_cte
END
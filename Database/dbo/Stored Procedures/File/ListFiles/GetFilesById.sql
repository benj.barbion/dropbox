﻿CREATE PROCEDURE [dbo].[GetFilesById]
	@FileId UNIQUEIDENTIFIER
AS
	SELECT f.Id, f.FileId, f.Name AS FileName, f.CreateDate, f.IdCreator AS Owner, fr.Size, f.MimeType, f.IsDeleted AS Trashed, f2.FileId AS ParentDirectory, f.IsFolder
	FROM dbo.[File] f
	LEFT JOIN dbo.[File] f2 ON f2.Id = f.IdParentFolder
	LEFT JOIN dbo.[FileRevision] fr ON fr.RevisionOf = f.Id AND fr.Id = f.CurrentRevisionId
	WHERE f.FileId = @FileId
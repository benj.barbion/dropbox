﻿CREATE PROCEDURE [dbo].[AddFolder]
	@FolderId UNIQUEIDENTIFIER,
	@FolderName NVARCHAR(200),
	@CreatorId NVARCHAR(450),
	@FolderParent UNIQUEIDENTIFIER = NULL
AS
BEGIN	
	DECLARE @IdParentFolder INT = NULL
	DECLARE @MimeType NVARCHAR (150) = "application/folder"

	IF @FolderParent IS NOT NULL 
		SET @IdParentFolder = (SELECT Id FROM dbo.[File] WHERE FileId = @FolderParent)

	INSERT INTO [dbo].[File](FileId, Name, IsFolder, IdCreator, MimeType, IdParentFolder) VALUES (@FolderId, @FolderName, 1, @CreatorId, @MimeType, @IdParentFolder)
END
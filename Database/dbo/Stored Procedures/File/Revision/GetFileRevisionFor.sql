﻿CREATE PROCEDURE [dbo].[GetFileRevisionFor]
	@FileId UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE @Id INT = (SELECT Id FROM dbo.[File] WHERE FileId = @FileId)

	SELECT fr.StorageId AS FileId, fr.CreateDate, asp.UserName AS Owner, fr.Size
	FROM dbo.[FileRevision] fr
	LEFT JOIN dbo.[File] f ON fr.Id = f.CurrentRevisionId
	LEFT JOIN dbo.[AspNetUsers] asp ON asp.Id = fr.IdCreator
	WHERE fr.RevisionOf = @Id AND fr.IsDeleted = 0
	ORDER BY fr.Id DESC
END
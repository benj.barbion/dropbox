﻿CREATE PROCEDURE [dbo].[AddFileRevision]
	@FileId UNIQUEIDENTIFIER, 
	@RevisionOf UNIQUEIDENTIFIER, 
	@CreatorId NVARCHAR(450),
	@Size BIGINT
AS
BEGIN	
	SET IMPLICIT_TRANSACTIONS ON

	BEGIN TRY
		DECLARE @RevisionOfId INT = (SELECT Id FROM [dbo].[File] WHERE FileId = @RevisionOf)

		INSERT INTO [dbo].[FileRevision](StorageId, Size, IdCreator, RevisionOf) VALUES (@FileId, @Size, @CreatorId, @RevisionOfId)

		UPDATE [dbo].[File] SET CurrentRevisionId = SCOPE_IDENTITY() WHERE Id = @RevisionOfId
		
		COMMIT 		
	END TRY

	BEGIN CATCH
		ROLLBACK
	END CATCH	
END

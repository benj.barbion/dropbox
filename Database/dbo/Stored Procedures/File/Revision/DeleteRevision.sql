﻿CREATE PROCEDURE [dbo].[DeleteRevision]
	@RevisionId UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE @RevisionIdINT INT = (SELECT Id FROM [dbo].[FileRevision] WHERE StorageId = @RevisionId)
	DECLARE @ParentId INT = (SELECT RevisionOf FROM [dbo].[FileRevision] WHERE Id = @RevisionIdINT)
	DECLARE @CurrentRevisionId INT = (SELECT CurrentRevisionId FROM [dbo].[File] WHERE Id = @ParentId)

	DELETE FROM dbo.FileRevision WHERE Id = @RevisionIdINT

	IF @CurrentRevisionId = @RevisionIdINT
		UPDATE [dbo].[File] 
		SET CurrentRevisionId = (SELECT TOP 1 Id FROM dbo.FileRevision WHERE RevisionOf = @ParentId)
		WHERE Id = @ParentId
END
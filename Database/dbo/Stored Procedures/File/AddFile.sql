﻿CREATE PROCEDURE [dbo].[AddFile]
	@FileId UNIQUEIDENTIFIER, 
	@FileName NVARCHAR(200), 
	@CreatorId NVARCHAR(450), 	
	@MimeType NVARCHAR(150),
	@Size BIGINT,
	@Directory UNIQUEIDENTIFIER = NULL
AS
BEGIN	
	SET IMPLICIT_TRANSACTIONS ON

	BEGIN TRY
		DECLARE @IdParentFolder INT = NULL
		DECLARE @RevisionOf INT = NULL

		IF @Directory IS NOT NULL 
			SET @IdParentFolder = (SELECT Id FROM dbo.[File] WHERE FileId = @Directory)

		INSERT INTO [dbo].[File](FileId, Name, IdCreator, MimeType, IdParentFolder) VALUES (@FileId, @FileName, @CreatorId, @MimeType, @IdParentFolder)
		
		SET @RevisionOf = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[FileRevision](StorageId, Size, IdCreator, RevisionOf) VALUES (@FileId, @Size, @CreatorId, @RevisionOf)

		UPDATE [dbo].[File] SET CurrentRevisionId = SCOPE_IDENTITY() WHERE Id = @RevisionOf

		COMMIT 		
	END TRY

	BEGIN CATCH
		ROLLBACK
	END CATCH	
END
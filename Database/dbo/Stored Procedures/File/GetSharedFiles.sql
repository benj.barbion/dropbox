﻿CREATE PROCEDURE [dbo].[GetSharedFiles]
	@UserId NVARCHAR(450) = NULL
AS
BEGIN
	SELECT f.FileId
	FROM dbo.FileSharing fs
	INNER JOIN dbo.[File] f ON f.Id = fs.IdToShare 
	WHERE f.IdCreator != @UserId 
		AND (fs.IdUserTo = @UserId 
				OR fs.IdGroupTo 
					IN (SELECT IdGroup FROM dbo.GroupUser WHERE IdUser = @UserId))
END
﻿CREATE PROCEDURE [dbo].[DeleteFile]
	@FileId UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE @FileIdINT INT = (SELECT Id FROM [dbo].[File] WHERE FileId = @FileId)

	--have to create multiple delete for cycle delete in MS
	DELETE FROM dbo.FileSharing WHERE IdToShare = @FileIdINT
	DELETE FROM dbo.FileRevision WHERE RevisionOf = @FileIdINT
	DELETE FROM dbo.[File] WHERE IdParentFolder = @FileIdINT
	DELETE FROM dbo.[File] WHERE Id = @FileIdINT
END
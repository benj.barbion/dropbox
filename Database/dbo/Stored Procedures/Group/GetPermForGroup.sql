﻿CREATE PROCEDURE [dbo].[GetPermForGroup]
	@GroupId UNIQUEIDENTIFIER,
	@UserId NVARCHAR(450)
AS
	SELECT gp.Name
	FROM dbo.[Group] g
	INNER JOIN dbo.GroupUser gu ON g.Id = gu.IdGroup
	INNER JOIN dbo.GroupRolePermission grp ON grp.IdRole = gu.IdRole
	INNER JOIN dbo.GroupPermission gp ON gp.Id = grp.IdPermission
	WHERE g.GroupId = @GroupId AND gu.IdUser = @UserId
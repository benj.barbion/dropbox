﻿CREATE PROCEDURE [dbo].[RemoveGroup]
	@GroupId UNIQUEIDENTIFIER
AS
	DELETE FROM [dbo].[Group] WHERE GroupId = @GroupId

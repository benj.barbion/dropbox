﻿CREATE PROCEDURE [dbo].[GetGroup]
	@UserId NVARCHAR(450)
AS
	SELECT DISTINCT g.* 
	FROM dbo.[Group] g
	INNER JOIN dbo.GroupUser gu ON gu.IdGroup = g.Id
	WHERE gu.IdUser = @UserId
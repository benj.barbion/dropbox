﻿CREATE PROCEDURE [dbo].[AddGroup]
	@GroupName NVARCHAR(200),
	@GroupId UNIQUEIDENTIFIER,
	@CreatorId NVARCHAR(450)
AS
BEGIN
	SET IMPLICIT_TRANSACTIONS ON
	
	BEGIN TRY
		INSERT INTO [dbo].[Group](GroupId, Name, IdCreator) VALUES (@GroupId, @GroupName, @CreatorId)
		
		DECLARE @LastInsertId INT = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].GroupUser(IdGroup, IdUser, IdRole) VALUES (@LastInsertId, @CreatorId, 4)

		COMMIT
	END TRY

	BEGIN CATCH
		ROLLBACK
	END CATCH
END
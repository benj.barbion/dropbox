﻿CREATE PROCEDURE [dbo].[GetUserRole]
	@Id NVARCHAR(128)
AS
BEGIN
	SELECT ur.RoleId, r.Name As RoleName, STRING_AGG(r.Name, ',') AS Permissions
	FROM dbo.AspNetUsers u
	INNER JOIN dbo.AspNetUserRoles ur ON u.Id = ur.UserId
	INNER JOIN dbo.AspNetRoles r ON r.Id = ur.RoleId
	WHERE u.Id = @Id
	GROUP BY ur.RoleId, r.Name
END
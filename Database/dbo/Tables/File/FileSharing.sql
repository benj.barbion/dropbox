﻿CREATE TABLE [dbo].[FileSharing] (
    [Id] INT IDENTITY PRIMARY KEY,
    [IdToShare] INT NOT NULL,
    [IdUserFrom] NVARCHAR (450) NOT NULL,
    [IdUserTo] NVARCHAR (450),
    [IdGroupTo] INT,
    [IdRole] INT NOT NULL,
    FOREIGN KEY ([IdToShare]) REFERENCES [dbo].[File] ([Id]),
    FOREIGN KEY ([IdUserFrom]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    FOREIGN KEY ([IdUserTo]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    FOREIGN KEY ([IdGroupTo]) REFERENCES [dbo].[Group] ([Id]),
    FOREIGN KEY ([IdRole]) REFERENCES [dbo].[FileRole] ([Id]),
    UNIQUE NONCLUSTERED ([IdToShare], [IdUserFrom], [IdUserTo]), 
    UNIQUE NONCLUSTERED ([IdToShare], [IdUserFrom], [IdGroupTo])
)
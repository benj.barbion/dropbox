﻿CREATE TABLE [dbo].[FileRolePermission] (
    [IdRole]       INT NOT NULL,
    [IdPermission] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([IdRole] ASC, [IdPermission] ASC),
    FOREIGN KEY ([IdPermission]) REFERENCES [dbo].[FilePermission] ([Id]),
    FOREIGN KEY ([IdRole]) REFERENCES [dbo].[FileRole] ([Id])
)
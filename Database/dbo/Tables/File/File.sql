﻿CREATE TABLE [dbo].[File] (
    [Id] INT IDENTITY PRIMARY KEY,
    [FileId] UNIQUEIDENTIFIER DEFAULT NEWID() NOT NULL,
    [Name] NVARCHAR (200) NOT NULL,
    [CreateDate] DATETIME DEFAULT getdate() NOT NULL,   
    [IsDeleted] BIT DEFAULT 0 NOT NULL,
    [IsFolder] BIT DEFAULT 0 NOT NULL,
    [IdCreator] NVARCHAR (450) NOT NULL,
    [MimeType] NVARCHAR (150) NOT NULL,
    [CurrentRevisionId] INT,
    [IdParentFolder] INT,
    FOREIGN KEY ([IdCreator]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    FOREIGN KEY ([CurrentRevisionId]) REFERENCES [dbo].[FileRevision] ([Id]) ON DELETE SET NULL,
    FOREIGN KEY ([IdParentFolder]) REFERENCES [dbo].[File] ([Id]),
    UNIQUE NONCLUSTERED ([FileId])
)
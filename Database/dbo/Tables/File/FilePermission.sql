﻿CREATE TABLE [dbo].[FilePermission] (
    [Id] INT IDENTITY PRIMARY KEY,
    [Name] VARCHAR (50) NOT NULL
)

--canRead, canModify, canSeeHistoric, cadAddToFolder, canShare, canTrash, canDelete, canDownload
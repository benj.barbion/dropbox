﻿CREATE TABLE [dbo].[FileRevision] (
    [Id] INT IDENTITY PRIMARY KEY,
    [StorageId] UNIQUEIDENTIFIER DEFAULT NEWID() NOT NULL,
    [CreateDate] DATETIME DEFAULT GETDATE() NOT NULL,
    [IsDeleted] BIT DEFAULT 0 NOT NULL,
    [Size] BIGINT NULL,
    [IdCreator] NVARCHAR (450) NOT NULL,
    [RevisionOf] INT NOT NULL,
    FOREIGN KEY ([RevisionOf]) REFERENCES [dbo].[File] ([Id]),
    FOREIGN KEY ([IdCreator]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
)
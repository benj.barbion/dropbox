﻿CREATE TABLE [dbo].[FileRole] (
	[Id] INT IDENTITY PRIMARY KEY,
    [Name] VARCHAR (50) NOT NULL,
	[PriorityLevel] INT NOT NULL DEFAULT 0
)

--owner (Leval 3), editor (Leval 2), viewer (Leval 1)
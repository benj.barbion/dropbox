﻿CREATE TABLE [dbo].[GroupRolePermission] (
    [IdRole]       INT NOT NULL,
    [IdPermission] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([IdRole] ASC, [IdPermission] ASC),
    FOREIGN KEY ([IdPermission]) REFERENCES [dbo].[GroupPermission] ([Id]) ON DELETE CASCADE,
    FOREIGN KEY ([IdRole]) REFERENCES [dbo].[GroupRole] ([Id]) ON DELETE CASCADE
)

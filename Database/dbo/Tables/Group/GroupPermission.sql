﻿CREATE TABLE [dbo].[GroupPermission] 
(
    [Id] INT IDENTITY PRIMARY KEY,
    [Name] VARCHAR (50) NOT NULL
);

--canDeleteGroup, canAddUser, canRemoveUser, canJoin, canShareInside
﻿CREATE TABLE [dbo].[GroupRole] 
(
	[Id] INT IDENTITY PRIMARY KEY,
    [Name] VARCHAR (50) NOT NULL,
	[PriorityLevel] INT NOT NULL
);

--anonymous (Id = 1), member (Id = 2), manager (Id = 3), owner (Id = 4)  
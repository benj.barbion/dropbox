﻿CREATE TABLE [dbo].[GroupUser] 
(
    [IdGroup] INT NOT NULL,
    [IdUser]  NVARCHAR (450) NOT NULL,
    [IdRole]  INT NOT NULL DEFAULT 2,
    PRIMARY KEY CLUSTERED ([IdGroup] ASC, [IdUser] ASC),
    FOREIGN KEY ([IdGroup]) REFERENCES [dbo].[Group] ([Id]) ON DELETE CASCADE,
    FOREIGN KEY ([IdUser]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE,
    FOREIGN KEY ([IdRole]) REFERENCES [dbo].[GroupRole] ([Id]) ON DELETE SET DEFAULT
);
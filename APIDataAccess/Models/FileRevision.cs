﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIDataAccess.Models
{
    public class FileRevision
    {
        public Guid FileId { get; set; }
        public DateTime CreateDate { get; set; }
        public string Owner { get; set; }
        public long Size { get; set; }
    }
}

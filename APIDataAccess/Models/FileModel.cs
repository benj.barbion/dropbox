﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIDataAccess.Models
{
    public class FileModel : IFileModel
    {
        public Guid FileId { get; set; }
        public string FileName { get; set; }
        public DateTime CreateDate { get; set; }
        public string Owner { get; set; }
        public long Size { get; set; }
        public string MimeType { get; set; }
        public bool Trashed { get; set; }
        public Guid? ParentDirectory { get; set; }
        public Guid? RevisionOf { get; set; }
        public bool IsFolder { get; set; }

        //Workaround to let Dapper create the object inside FileRepository
        public FileModel()
        {
        }

        private FileModel(Builder builder)
        {
            this.FileId = builder.FileId;
            this.FileName = builder.FileName;
            this.Owner = builder.Owner;
            this.Size = builder.Size;
            this.MimeType = builder.MimeType;
            this.ParentDirectory = builder.ParentDirectory;
            this.RevisionOf = builder.RevisionOf;
        }

        public class Builder
        {
            internal Guid FileId { get; set; }
            internal string FileName { get; set; }
            internal string Owner { get; set; }
            internal long Size { get; set; }
            internal string MimeType { get; set; }
            internal Guid? ParentDirectory { get; set; }
            internal Guid? RevisionOf { get; set; }

            public Builder SetRevisionOf(Guid? RevisionOf)
            {
                this.RevisionOf = RevisionOf;
                return this;
            }

            public Builder SetFileName(string fileName)
            {
                this.FileName = fileName;
                return this;
            }

            public Builder SetFileId(Guid fileId)
            {
                this.FileId = fileId;
                return this;
            }

            public Builder SetOwner(string creatorId)
            {
                this.Owner = creatorId;
                return this;
            }

            public Builder SetMimeType(string mimeType)
            {
                this.MimeType = mimeType;
                return this;
            }

            public Builder SetSize(long size)
            {
                this.Size = size;
                return this;
            }

            public Builder SetParentDirectory(Guid? parentDirectory)
            {
                this.ParentDirectory = parentDirectory;
                return this;
            }

            public FileModel Build()
            {
                return new FileModel(this);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIDataAccess.Models
{
    public class FileRole
    {
        public string Name { get; set; }
        public int PriorityLevel { get; set; }
    }
}

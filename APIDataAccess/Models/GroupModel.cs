﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIDataAccess.Models
{
    public class GroupModel
    {
        public string GroupId { get; set; }
        public string Name { get; set; }
        public string IdCreator { get; set; }
    }
}

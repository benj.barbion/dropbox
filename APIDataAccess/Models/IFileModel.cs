﻿using System;

namespace APIDataAccess.Models
{
    public interface IFileModel
    {
        DateTime CreateDate { get; set; }
        Guid FileId { get; set; }
        string FileName { get; set; }
        bool IsFolder { get; set; }
        string MimeType { get; set; }
        string Owner { get; set; }
        Guid? ParentDirectory { get; set; }
        Guid? RevisionOf { get; set; }
        long Size { get; set; }
        bool Trashed { get; set; }
    }
}
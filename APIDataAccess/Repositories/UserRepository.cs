﻿using APIDataAccess.Models;
using System.Collections.Generic;
using System.Linq;

namespace APIDataAccess.DataAccess
{
    public class UserRepository : IUserRepository
    {
        private readonly IDataRepository sql;

        public UserRepository(IDataRepository sql) => this.sql = sql;

        public User GetUser(string id)
        {
            var output = sql.LoadData<User, dynamic>("dbo.GetUserById", new { Id = id }, "DropboxDatabase");

            var user = output.FirstOrDefault();
            if (user is null)
                return user;

            return user;
        }
    }
}

﻿using APIDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace APIDataAccess.DataAccess
{
    public interface IFileRepository
    {
        List<FileModel> GetAllBy(Expression<Func<FileModel, bool>> predicate, string user);
        FileModel GetById(Guid fileId);
        List<FileModel> GetByName(string fileName, Guid? parent);
        List<FileModel> GetByNameRoot(string fileName, string userId);
        List<Guid> GetSharedFiles(string user);    
        List<string> GetPerms(Guid fileId, string userId);
        int SavePermission(string userIdFrom, string userIdTo, string role, Guid fileId, bool shareWithGroup = false);      
        int SaveFolder(FileModel file);
        int SaveFile(FileModel file);
        int SaveRevision(FileModel file);             
        List<FileRevision> GetAllRevisionFor(Guid fileId);
        int DeleteFile(Guid id);
        int DeleteRevision(Guid revisionId);
        List<FileRole> GetRoles();
    }
}
﻿using APIDataAccess.Models;
using System;
using System.Collections.Generic;

namespace APIDataAccess.Repositories
{
    public interface IGroupRepository
    {
        int SaveGroup(Guid groupId, string groupName, string creatorId);
        int DeleteGroup(Guid groupId);
        List<GroupModel> GetGroup(string userId);
        List<string> GetPerms(Guid groupdId, string userId);
        int AddUserToGroup(Guid groupId, string userId);
        int RemoveUserFromGroup(Guid groupId, string userId);
        int UpdateUserRole(Guid groupId, string userId, string role);
    }
}
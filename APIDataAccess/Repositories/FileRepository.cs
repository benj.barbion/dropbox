﻿using APIDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace APIDataAccess.DataAccess
{
    public class FileRepository : IFileRepository
    {
        private readonly IDataRepository sql;

        public FileRepository(IDataRepository sql) => this.sql = sql;

        public List<FileModel> GetAllBy(Expression<Func<FileModel, bool>> predicate, string user)
        {
            //ATTENTION : requête chronophage étant donné l'utilisation des procédures stockées et dapper
            //Cela serait bien plus efficace si tout avait été géré via entity framework (le where effectuerait un filtre efficace)
            //MAIS semblait tout de même intéressant à montrer car -->
            //  Démontre comment il est possible de pouvoir accéder à la donnée demandée si ENTITY FRAMEWORK était utilisé 
            //  Evite l'utilisation de multiples overloading selon l'information demandée
            //  Logique des prédicats puissante en utilisant les Func : new Func<FileModel, bool>(x => x.FileId == Guid.Empty);

            var test = sql.LoadData<FileModel, dynamic>("dbo.GetAllFiles", new { UserId = user }, "DropboxDatabase");

            return test
                    .Where(predicate.Compile())
                    .ToList();
        }

        public FileModel GetById(Guid fileId)
        {
            var param = new { FileId = fileId };

            var output = this.sql.LoadData<FileModel, dynamic>("dbo.GetFilesById", param, "DropboxDatabase");

            return output.FirstOrDefault();
        }

        public List<FileModel> GetByName(string fileName, Guid? parent)
        {
            var param = new { FileName = fileName, Parent = parent };

            var output = this.sql.LoadData<FileModel, dynamic>("dbo.GetFilesByName", param, "DropboxDatabase");

            return output;
        }

        public List<FileModel> GetByNameRoot(string fileName, string user)
        {
            var param = new { FileName = fileName, UserId = user };

            var output = this.sql.LoadData<FileModel, dynamic>("dbo.GetFilesByNameRoot", param, "DropboxDatabase");

            return output;
        }

        public List<Guid> GetSharedFiles(string user)
        {
            var param = new { UserId = user };

            var output = this.sql.LoadData<Guid, dynamic>("dbo.GetSharedFiles", param, "DropboxDatabase");

            return output;
        }

        public List<string> GetPerms(Guid fileId, string userId)
        {
            var param = new { FileId = fileId, UserId = userId };

            var output = this.sql.LoadData<string, dynamic>("dbo.GetPermForFile", param, "DropboxDatabase");

            return output;
        }     

        public int SavePermission(string userIdFrom, string userIdTo, string role, Guid fileId, bool shareWithGroup = false)
        {
            var param = new { UserIdFrom = userIdFrom, UserIdTo = userIdTo, Role = role, FileId = fileId, SharedWithGroup = shareWithGroup };

            return this.sql.SaveData<dynamic>("dbo.AddFilePermission", param, "DropboxDatabase");
        }

        public int SaveFolder(FileModel file)
        {
            var param = new { FolderId = file.FileId, FolderName = file.FileName, CreatorId = file.Owner, FolderParent = file.ParentDirectory };

            return this.sql.SaveData<dynamic>("dbo.AddFolder", param, "DropboxDatabase");
        }

        public int SaveFile(FileModel file)
        {
            var param = new { file.FileId, file.FileName, CreatorId = file.Owner, file.MimeType, file.Size, Directory = file.ParentDirectory };

            return this.sql.SaveData<dynamic>("dbo.AddFile", param, "DropboxDatabase");
        }

        public int SaveRevision(FileModel file)
        {
            var param = new { file.FileId, file.RevisionOf, CreatorId = file.Owner, file.Size };

            return this.sql.SaveData<dynamic>("dbo.AddFileRevision", param, "DropboxDatabase");
        }

        public List<FileRevision> GetAllRevisionFor(Guid fileId)
        {
            var param = new { FileId = fileId };

            var output = this.sql.LoadData<FileRevision, dynamic>("dbo.GetFileRevisionFor", param, "DropboxDatabase");

            return output;
        }

        public int DeleteFile(Guid id)
        {
            var param = new { FileId = id };

            return this.sql.SaveData<dynamic>("dbo.DeleteFile", param, "DropboxDatabase");
        }

        public List<FileRole> GetRoles()
        {
            return this.sql.LoadData<FileRole, dynamic>("dbo.GetFilesRoles", new{ }, "DropboxDatabase");
        }

        public int DeleteRevision(Guid revisionId)
        {
            var param = new { RevisionId = revisionId };

            return this.sql.SaveData<dynamic>("dbo.DeleteRevision", param, "DropboxDatabase");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIDataAccess
{
    public interface IDataRepository
    {
        List<T> LoadData<T, U>(string storedProcedure, U parameters, string name);
        int SaveData<T>(string storedProcedure, T parameters, string name);
    }
}

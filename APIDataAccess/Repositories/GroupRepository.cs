﻿using APIDataAccess.Models;
using System;
using System.Collections.Generic;

namespace APIDataAccess.Repositories
{
    public class GroupRepository : IGroupRepository
    {
        private readonly IDataRepository sql;

        public GroupRepository(IDataRepository sql) => this.sql = sql;

        public int SaveGroup(Guid groupId, string groupName, string creatorId)
        {
            var param = new { GroupName = groupName, GroupId = groupId, CreatorId = creatorId };

            return this.sql.SaveData<dynamic>("dbo.AddGroup", param, "DropboxDatabase");
        }

        public int DeleteGroup(Guid groupId)
        {
            var param = new { GroupId = groupId };

            return this.sql.SaveData<dynamic>("dbo.RemoveGroup", param, "DropboxDatabase");
        }

        public List<string> GetPerms(Guid groupdId, string userId)
        {
            var param = new { GroupId = groupdId, UserId = userId };

            var output = sql.LoadData<string, dynamic>("dbo.GetPermForGroup", param, "DropboxDatabase");

            return output;
        }

        public int AddUserToGroup(Guid groupId, string userId)
        {
            var param = new { GroupId = groupId, UserId = userId };

            return this.sql.SaveData<dynamic>("dbo.AddUserToGroup", param, "DropboxDatabase");
        }

        public int RemoveUserFromGroup(Guid groupId, string userId)
        {
            throw new NotImplementedException();
        }

        public int UpdateUserRole(Guid groupId, string userId, string role)
        {
            throw new NotImplementedException();
        }

        public List<GroupModel> GetGroup(string userId)
        {
            var param = new { UserId = userId };

            var output = sql.LoadData<GroupModel, dynamic>("dbo.GetGroup", param, "DropboxDatabase");

            return output;
        }
    }
}

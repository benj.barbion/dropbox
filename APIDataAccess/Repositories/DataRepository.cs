﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace APIDataAccess
{
    public class DataRepository : IDataRepository
    {
        private readonly IConfiguration config;

        public DataRepository(IConfiguration config) => this.config = config;

        private string GetConnectionString(string name) => this.config.GetConnectionString(name);

        public List<T> LoadData<T, U>(string storedProcedure, U parameters, string name)
        {
            //Automatically use pooling
            using var connection = new SqlConnection(this.GetConnectionString(name));

            return connection.Query<T>(storedProcedure, parameters, commandType: CommandType.StoredProcedure).ToList();
        }

        public int SaveData<T>(string storedProcedure, T parameters, string name)
        {
            using var connection = new SqlConnection(this.GetConnectionString(name));

            return connection.Execute(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
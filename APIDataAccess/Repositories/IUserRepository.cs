﻿using APIDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIDataAccess.DataAccess
{
    public interface IUserRepository
    {
        User GetUser(string id);
    }
}

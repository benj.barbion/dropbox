﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using ConsoleAppUI.Models.LoggedInUser;

namespace ConsoleAppUI.Api.UserEndPoint
{
    public class UserEndPoint : IUserEndPoint
    {
        private readonly IApiHelper apiHelper;
       
        public UserEndPoint(IApiHelper apiHelper) => this.apiHelper = apiHelper;
        
	public async Task Register(string email, string password, string confirmPassword)
	{
		var data = new { Email = email, Password = password, ConfirmPassword = confirmPassword };

            	using HttpResponseMessage response = await this.apiHelper.ApiClient.PostAsJsonAsynC("/api/User/Register", data);

            	if (!response.IsSuccessStatusCode)
			throw new Exception("Inscription impossible.");
	}

        public async Task<ILoggedInUser> GetMe()
        {
            using HttpResponseMessage response = await this.apiHelper.ApiClient.GetAsync("/api/User");

            return response.IsSuccessStatusCode ? await response.Content.ReadAsAsync<LoggedInUser>() : null;
        }

        public async Task<bool> ChangePassword(string oldPassword, string newPassword, string confirmPassword)
        {
            var data = new { OldPassword = oldPassword, NewPassword = newPassword, ConfirmPassword = confirmPassword };

            using HttpResponseMessage response = await this.apiHelper.ApiClient.PostAsJsonAsync("/api/User/ChangePassword", data);

            return response.IsSuccessStatusCode;
        }
    }
}

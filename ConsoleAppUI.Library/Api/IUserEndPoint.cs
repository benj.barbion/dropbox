﻿using ConsoleAppUI.Models.LoggedInUser;
using System.Threading.Tasks;

namespace ConsoleAppUI.Api.UserEndPoint
{
    public interface IUserEndPoint
    {
        Task Register(string email, string password, string confirmPassword);
        Task<bool> ChangePassword(string oldPassword, string newPassword, string confirmPassword);
        Task<ILoggedInUser> GetMe();
    }
}

﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ConsoleAppUI.Api.UserEndPoint
{
    public class ApiHelper : IApiHelper
    {
        public HttpClient ApiClient { get; private set; }    
       
        public ApiHelper()
        {
            this.ApiClient = new HttpClient 
            { 
                BaseAddress = new Uri(ConfigurationManager.AppSettings["api"]) 
            };
            
            this.ApiClient.DefaultRequestHeaders.Accept.Clear();
            this.ApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public void DefaultRequestHeaders(string token)
        {
            this.ApiClient.DefaultRequestHeaders.Clear();
            this.ApiClient.DefaultRequestHeaders.Accept.Clear();
            this.ApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.ApiClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");
        }
    }
}

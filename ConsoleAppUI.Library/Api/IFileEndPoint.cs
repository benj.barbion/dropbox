﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using APIDataAccess.Models;

namespace ConsoleAppUI.Api.UserEndPoint
{
    public interface IFileEndPoint
    {
        Task<List<FileModel>> ListFile(string listFile);
        Task<List<FileRevision>> GetAllRevisionFor(Guid fileId);
    }
}
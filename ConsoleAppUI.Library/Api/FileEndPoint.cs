﻿using APIDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ConsoleAppUI.Api.UserEndPoint
{
    public class FileEndPoint : IFileEndPoint
    {
        private readonly IApiHelper apiHelper;
        
        public FileEndPoint(IApiHelper apiHelper) => this.apiHelper = apiHelper;

        public async Task<List<FileModel>> ListFile(string listFile)
        {
            using HttpResponseMessage response = await apiHelper.ApiClient.GetAsync($"/api/File?{listFile}");

            var files = await response.Content.ReadAsAsync<List<FileModel>>();

            if (response.IsSuccessStatusCode)
                return files;

            return new List<FileModel>();
        }   

        public async Task<List<FileRevision>> GetAllRevisionFor(Guid fileId)
        {
            using HttpResponseMessage response = await this.apiHelper.ApiClient.GetAsync($"/api/File/{fileId}/revisions");

            if (!response.IsSuccessStatusCode)
                throw new Exception(response.ReasonPhrase);

            return await response.Content.ReadAsAsync<List<FileRevision>>();
        }
    }
}

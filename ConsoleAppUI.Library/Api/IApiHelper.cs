﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Headers;

namespace ConsoleAppUI.Api.UserEndPoint
{
    public interface IApiHelper
    {
        HttpClient ApiClient { get; }
        void DefaultRequestHeaders(string token);
        void ClearHeaders()
        {
            this.ApiClient.DefaultRequestHeaders.Clear(); 
            this.ApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
    }
}
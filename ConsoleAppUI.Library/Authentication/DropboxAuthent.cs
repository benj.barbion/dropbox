﻿using ConsoleAppUI.Models.LoggedInUser;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsoleAppUI.Api.Authentication
{
    public class DropboxAuthent : IDropboxAuthent
    {
        public IAuthentication Strategy { get; set; }

        private readonly ILoggedInUser loggedInUser;

        public DropboxAuthent(ILoggedInUser loggedInUser)
        {
            this.loggedInUser = loggedInUser ?? throw new ArgumentNullException(nameof(loggedInUser));
        }

        public async Task<ILoggedInUser> DoLogin(String username, String password)
        {
            if (this.loggedInUser.Token != null)
                throw new Exception("(DropboxAuthent) Déjà connecté.");

            if (this.Strategy is null)
                throw new Exception("(DropboxAuthent) No authent strategy.");

            var user = await this.Strategy.Login(username, password);

            this.loggedInUser.Id = user.Id;
            this.loggedInUser.Email = user.Email;
            this.loggedInUser.Token = user.Token;

            return this.loggedInUser;
        }

        public void DoLogout()
        {
            if (this.loggedInUser?.Token is null)
                throw new Exception("(DropboxAuthent) Vous n'êtes pas connecté.");

            this.loggedInUser?.Reset();
            this.Strategy?.Logout();
        }
    }
}
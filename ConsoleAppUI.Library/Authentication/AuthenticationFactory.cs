﻿using ConsoleAppUI.Api.UserEndPoint;
using DesktopUI.Library.Authentication;

namespace ConsoleAppUI.Api.Authentication
{
    public class AuthenticationFactory
    {
        private readonly IApiHelper apiHelper;
        private readonly IUserEndPoint userEndPoint;

        public AuthenticationFactory(IApiHelper apiHelper, IUserEndPoint userEndPoint)
        {
            this.apiHelper = apiHelper;
            this.userEndPoint = userEndPoint;
        }

        public IAuthentication CreateAuthentication(string type)
        {
            return type switch
            {
                nameof(ApiAuthentication) => new ApiAuthentication(apiHelper, userEndPoint),
                _ => null
            };
        }
    }
}

﻿using ConsoleAppUI.Api.Authentication;
using ConsoleAppUI.Api.UserEndPoint;
using ConsoleAppUI.Models.LoggedInUser;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;

namespace DesktopUI.Library.Authentication
{
    public class ApiAuthentication : IAuthentication
    {
        private readonly IApiHelper apiHelper;
        private readonly IUserEndPoint userEndPoint;

        public ApiAuthentication(IApiHelper apiHelper, IUserEndPoint userEndPoint)
        {
            this.apiHelper = apiHelper;
            this.userEndPoint = userEndPoint;
        }

        public async Task<ILoggedInUser> Login(string username, string password)
        {
            var loginDetails = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("password", password),
            });

            using HttpResponseMessage response = await this.apiHelper.ApiClient.PostAsync("/JWTToken", loginDetails);
            if (!response.IsSuccessStatusCode)
                throw new Exception("Connexion impossible.");

            var token = await response.Content.ReadAsAsync<string>();

            return await this.GetLoggedInUser(token);
        }

        private async Task<ILoggedInUser> GetLoggedInUser(string token)
        {
            this.apiHelper.DefaultRequestHeaders(token);

            var user = await this.userEndPoint.GetMe();
            if (user is null)
                throw new Exception("Utilisateur inconnu.");

            user.Token = token;

            return user;
        }

        public void Logout() => this.apiHelper.ClearHeaders();
    }
}

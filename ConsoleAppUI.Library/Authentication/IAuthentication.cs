﻿using ConsoleAppUI.Models.LoggedInUser;
using System.Threading.Tasks;

namespace ConsoleAppUI.Api.Authentication
{
    public interface IAuthentication
    {
        Task<ILoggedInUser> Login(string username, string password);
        void Logout();
    }
}

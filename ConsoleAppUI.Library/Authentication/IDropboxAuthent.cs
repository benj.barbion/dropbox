﻿using ConsoleAppUI.Models.LoggedInUser;
using System.Threading.Tasks;

namespace ConsoleAppUI.Api.Authentication
{
    public interface IDropboxAuthent
    {
        IAuthentication Strategy { get; set; }
        Task<ILoggedInUser> DoLogin(string username, string password);
        void DoLogout();
    }
}
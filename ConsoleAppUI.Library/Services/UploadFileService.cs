﻿using ConsoleAppUI.Api.UserEndPoint;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ConsoleAppUI.Library.Services
{
    //Was being used inside UI with drag and drop
    public class UploadFileService : IUploadFileService
    {
        private readonly IApiHelper apiHelper;

        public UploadFileService(IApiHelper apiHelper) => this.apiHelper = apiHelper;

        public async Task Upload(List<string> filePaths, Guid? parentFolder = null)
        {
            foreach (var path in filePaths)
            {
                if (Directory.Exists(path))
                    await this.ProcessDirectory(path, parentFolder);
                else if (File.Exists(path))
                    await this.ProcessFile(path, parentFolder);
            }
        }

        private async Task ProcessDirectory(string path, Guid? parentFolder = null)
        {
            var test = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("MimeType", "application/folder"),
                new KeyValuePair<string, string>("FileName", new DirectoryInfo(path).Name)
            };

            if (parentFolder != null)
                test.Add(new KeyValuePair<string, string>("FileName", new DirectoryInfo(path).Name));

            using HttpResponseMessage createFolder = await this.apiHelper.ApiClient.PostAsync("/api/File", new FormUrlEncodedContent(test));
            if (!createFolder.IsSuccessStatusCode)
                throw new Exception(createFolder.ReasonPhrase);

            var folderGuid = await createFolder.Content.ReadAsAsync<Guid>();

            foreach (var file in Directory.GetFiles(path))
                await this.ProcessFile(file, folderGuid);
            
            await this.Upload(Directory.GetDirectories(path).ToList(), folderGuid);
        }

        private async Task ProcessFile(string path, Guid? parentFolder = null)
        {
            var multipartFormDataContent = new MultipartFormDataContent
            {
                { new ByteArrayContent(File.ReadAllBytes(path)), "files", Path.GetFileName(path) }
            };

            if (parentFolder != null)
                multipartFormDataContent.Add(new StringContent(parentFolder.ToString()), "FromFileId");

            using HttpResponseMessage response = await this.apiHelper.ApiClient.PostAsync($"/api/File", multipartFormDataContent);

            if (!response.IsSuccessStatusCode)
                throw new Exception(response.ReasonPhrase);
        }
    }
}

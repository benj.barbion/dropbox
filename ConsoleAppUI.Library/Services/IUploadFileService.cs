﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsoleAppUI.Library.Services
{
    public interface IUploadFileService
    {
        Task Upload(List<string> filePaths, Guid? parentFolder = null);
    }
}
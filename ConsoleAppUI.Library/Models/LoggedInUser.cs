﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppUI.Models.LoggedInUser
{
    public class LoggedInUser : ILoggedInUser
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }

        public void Reset()
        {
            this.Id = null;
            this.Email = null;
            this.Token = null;
        }
    }
}
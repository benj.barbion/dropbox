﻿using System;
using System.Collections.Generic;

namespace ConsoleAppUI.Models.LoggedInUser
{
    public interface ILoggedInUser
    {
        string Id { get; set; }
        string Email { get; set; }
        string Token { get; set; }
        void Reset();
    }
}